// - What directive is used by Node.js in loading the modules it needs?
	//answer: require()

// - What Node.js module contains a method for server creation?
	//answer: http

// - What is the method of the http object responsible for creating a server using Node.js?
	//answer: createServer()

// - What method of the response object allows us to set status codes and content types?
	//answer: response.writeHead()

// - Where will console.log() output its contents when run in Node.js?
	//answer: terminal/git bash or in the browser

// - What property of the request object contains the address's endpoint?
	//answer: response.end()



const http = require("http")

const port = 3000

const server = http.createServer((request, response) => {
	if (request.url === "/login") {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('You are in the login page')
	}
	else{
		response.writeHead (404, {'Content-Type': 'text/plain'});
		response.end(`ERROR: 404 PAGE NOT FOUND!`)
	}
})
server.listen(port)

console.log(`server now accessible at localhost:${port}`)
